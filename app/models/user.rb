class User < ApplicationRecord
    has_secure_password
    has_many :recipes, dependent: :destroy
    has_many :comments, dependent: :destroy
    has_many :comment_upvotes, dependent: :destroy

    validates_presence_of :email
    validates_uniqueness_of :email
end
