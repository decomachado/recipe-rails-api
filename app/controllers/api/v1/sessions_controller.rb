module Api
    module V1
        class SessionsController < ApplicationController
            include CurrentUserConcern

            def create
                if @current_user
                    render json: {
                        status: 200,
                        message: "Already logged in",
                        user: @current_user
                    }
                else
                    user = User
                        .find_by_email(params[:user][:email])
                        .try(:authenticate, params[:user][:password])
    
                    if user 
                        session[:user_id] = user.id
                        session[:expires_at] = Time.current + 10.minutes 
                        render json: {
                            status: :created,
                            logged_in: true,
                            user: user
                        }
                    else
                        render json: {
                            status: 401
                        }
                    end
                end
            end

            def logged_in 
                if @current_user
                    if Time.current > session[:expires_at]
                        reset_session
                    else
                        render json: {
                        logged_in: true,
                        user: @current_user
                    }
                        session[:expires_at] = Time.current + 10.minutes
                    end
                    
                else
                    render json: {
                        logged_in: false
                    }
                end
            end

            def logout
                reset_session
                render json: { status: 200, logged_in: false }
            end
        end
    end
end