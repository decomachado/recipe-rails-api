module Api
    module V1
        class UsersController < ApplicationController
            include CurrentUserConcern

            def create
                user = User.create!(
                    first_name: params[:user][:first_name],
                    last_name: params[:user][:last_name],
                    email: params[:user][:email],
                    password: params[:user][:password],
                    password_confirmation: params[:user][:password],
                    birth: params[:user][:birth],
                )

                if user 
                    session[:user_id] = user.id
                    session[:expires_at] = Time.current + 10.minutes
                    render json: {
                        status: :created,
                        user: user
                    }
                else
                    render json: {
                        status: 500
                    }
                end
            end

            def destroy
                if @current_user 
                    if Time.current > session[:expires_at]
                        reset_session
                        render json: {
                            logged_in: false
                        }
                    else
                        if User.find(params[:id]) == @current_user
                            reset_session
                            user.destroy
                            render json: {
                                status: 200,
                                message: "User deleted"
                            }
                        else
                            render json: {
                                status: 401,
                                message: "Unauthorized"
                            }
                            session[:expires_at] = Time.current + 10.minutes
                        end
                    end
                else
                    render json: {
                        status: 401,
                        logged_in: false
                    }
                end
            end

            def show 
                if @current_user 
                    session[:expires_at] = Time.current + 10.minutes
                end

                user = User.find(params[:id])
                if user 
                    render json: {
                        status: 200,
                        data: {
                            user: user,
                            recipes: user.recipes
                        } 
                    }
                else 
                    render json: {
                        status: 404
                    }
                end
            end

            def index 
                if @current_user 
                    session[:expires_at] = Time.current + 10.minutes
                end
                
                users = User.order('first_name ASC')

                if users 
                    render json: {
                        status: 200,
                        data: users
                    }
                else
                    render json: {
                        status: 500,
                    }
                end
            end

            def update 
                if @current_user
                    user = User.find(params[:id])
                    if @current_user == user
                        if params[:user][:first_name]
                            user.update(:first_name => params[:user][:first_name])
                        end

                        if params[:user][:last_name]
                            user.update(:last_name => params[:user][:last_name])
                        end

                        if params[:user][:password]
                            user.update(:password => params[:user][:password], :password_confirmation => params[:user][:password])
                        end

                        if params[:user][:email]
                            user.update(:email => params[:user][:email])
                        end

                        if params[:user][:birth]
                            user.update(:birth => params[:user][:birth])
                        end

                        render json: {
                            status: 200,
                            user: user
                        }
                    else
                        render json: {
                            status: 401,
                            message: "unauthorized"
                        }
                    end
                else
                    render json: {
                        status: 401,
                        message: "you have to be logged in to access this endpoint"
                    }
                end

                session[:expires_at] = Time.current + 10.minutes
            end
        end
    end
end
