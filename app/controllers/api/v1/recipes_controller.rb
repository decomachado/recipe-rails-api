module Api 
    module V1
        class RecipesController < ApplicationController
            include CurrentUserConcern

            def index
                if @current_user
                    session[:expires_at] = Time.current + 10.minutes
                end

                recipes = Recipe.order('id ASC')

                if recipes
                    render json: {
                        status: 200,
                        data: recipes
                    }
                else
                    render json: {
                        status: 500
                    }
                end   
            end

            def show
                if @current_user
                    session[:expires_at] = 10.minutes
                end
                
                recipe = Recipe.find(params[:id])
                if recipe
                    iArray = []
                    recipe.ingredients.each do |i|
                        iArray.push({
                            ingredient: i,
                            amount: recipe.recipe_ingredients.where(ingredient_id: i.id)[0].amount
                        })
                    end
                    render json: {
                        status: 200,
                        data: {
                            recipe: recipe,
                            ingredients: iArray 
                        }
                    }
                else
                    render json: {
                        status: 404
                    }
                end
            end

            def create 
                if @current_user
                    if Time.current > session[:expires_at]
                        reset_session
                        render json: {
                            logged_in: false,
                            message: "timeout"
                        }
                    else
                        recipe = Recipe.create({
                            user: @current_user,
                            title: params[:recipe][:title],
                            servings: params[:recipe][:servings],
                            prepare_time: params[:recipe][:prepare_time],
                            description: params[:recipe][:description]
                        })

                        ingredients = params[:recipe][:ingredients]

                        ingredients.each do |i|
                            pi = Ingredient.where(name: i[:name])

                            if pi.length() > 0
                                ingredient = pi[0]
                            else
                                ingredient = Ingredient.create!({
                                    name: i[:name],
                                    description: i[:description]
                                })
                            end

                            RecipeIngredient.create({
                                recipe: recipe,
                                ingredient: ingredient,
                                amount: i[:amount]
                            })
                        end

                        if recipe 
                            render json: {
                                status: :created,
                                recipe: recipe
                            }
                        else
                            render json: {
                                status: 422,
                                message: "unprocessable entity"
                            }
                        end

                        session[:expires_at] = Time.current + 10.minutes
                    end

                    session[:expires_at] = Time.current + 10.minutes
                else
                    render json: {
                        status: 401,
                        logged_in: false,
                        message: "You have to be logged in to create a new recipe"
                    }
                end
            end

            def destroy
                recipe = Recipe.find(params[:id])

                if @current_user
                    if @current_user == recipe.user
                        recipe.destroy
                        render json: {
                            status: 200,
                            message: "Recpie deleted"
                        }
                    else
                        render json: {
                            status: 401,
                            message: "Unauthorized"
                        }
                    end

                    session[:expires_at] = Time.current + 10.minutes

                else
                    render json: {
                        status: 401,
                        message: "You have to be logged in"
                    }
                end
            end
        end
    end
end

