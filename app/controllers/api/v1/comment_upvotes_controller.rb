module Api
    module V1
        class CommentUpvotesController < ApplicationController
            include CurrentUserConcern

            def create 
                if @current_user
                    if CommentUpvote.where(user: @current_user, comment: params[:upvote][:comment_id]).sum(:value)  == 0
                        if params[:upvote][:status] == true 
                                
                            cu = CommentUpvote.create({
                                value: 1,
                                user: @current_user,
                                comment: Comment.find(params[:upvote][:comment_id])
                            })

                            render json: {
                                status: 200,
                                upvote: cu
                            }
                        else
                            cu = CommentUpvote.create({
                                value: -1,
                                user: @current_user,
                                comment: Comment.find(params[:upvote][:comment_id])
                            })

                            render json: {
                                status: 200,
                                upvote: cu
                            }
                        end
                    else
                        render json: {
                            message: "already upvoted/downvoted"
                        }
                    end
                else
                    render json: {
                        status: 200,
                        message: "you have to be logged in"
                    }
                end
            end

            def destroy 
                if @current_user
                    upvote = CommentUpvote.find(params[:id])
                    if upvote
                        if @current_user == upvote.user
                            upvote.destroy
                            render json: {
                                status: 200,
                                message: "Upvote destroyed"
                            }
                        else
                            render json: {
                                status: 401,
                                message: "Unauthorized"
                            }
                        end
                    else
                        render json: {
                            status: 404,
                            message: "upvote not found"
                        }
                    end
                        
                else
                    render json: {
                        status: 401,
                        message: "you have to be logged in"
                    }
                end
            end
        end
    end
end