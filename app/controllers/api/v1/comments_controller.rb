module Api
    module V1
        class CommentsController < ApplicationController
            include CurrentUserConcern

            def create 
                if @current_user
                    comment = Comment.create({
                        title: params[:comment][:title],
                        content: params[:comment][:content],
                        rating: params[:comment][:rating],
                        user: @current_user,
                        recipe: Recipe.find(params[:comment][:recipe_id])
                    })

                    if comment 
                        render json: {
                            stauts: 200,
                            comment: comment
                        }
                    else
                        render json: {
                            status: 422,
                            message: "unprocessable entity"
                        }
                    end

                    session[:expires_at] = Time.current + 10.minutes
                else
                    render json: {
                        status: 401,
                        message: "You have to be logged in"
                    }
                end
            end

            def show

                if @current_user
                    session[:expires_at] = Time.current + 10.minutes
                end

                recipe = Recipe.find(params[:id])

                cArray = []

                if recipe
                    recipe.comments.each do |c|
                        cArray.push({
                            comment: c,
                            comment_upvotes: c.comment_upvotes
                        })
                    end

                    render json: [
                        status: 200,
                        comments: cArray
                    ]
                else
                    render json: {
                        status: 404,
                        message: "recipe not found"
                    }
                end
            end 

            def destroy
                if @current_user
                    comment = Comment.find(params[:id])
                    if comment
                        comment.destroy

                        render json: {
                            status: 200, 
                            message: "comment destroyed"
                        }
                    else
                        render json: {
                            status: 404,
                            message: "comment not Found"
                        }
                    end

                    session[:expires_at] = Time.current + 10.minutes
                else
                    render json: {
                        status: 401,
                        message: "you have to be logged in"
                    }
                end
            end

        end
    end
end