module Api
    module V1
        class IngredientsController < ApplicationController
            include CurrentUserConcern

            def index
                if @current_user
                    session[:expires_at] = Time.current + 10.minutes
                end

                ingredients = Ingredient.order('name ASC')
                
                if ingredients
                    render json: {
                        status: 200,
                        data: ingredients
                    }
                else
                    render json: {
                        status: 500
                    }
                end 
            end

            def show 
                if @current_user
                    session[:expires_at] = Time.current + 10.minutes
                end

                ingredient = Ingredient.find(params[:id])

                if ingredient 
                    render json: {
                        status: 200,
                        data: {
                            ingredient: ingredient,
                            recipes: ingredient.recipes
                        }
                    }
                else
                    render json: {
                        status: 404
                    }
                end
            end
        end
    end
end