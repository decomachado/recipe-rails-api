#!/bin/sh

docker-compose run app rails db:migrate
docker-compose run app rails db:seed