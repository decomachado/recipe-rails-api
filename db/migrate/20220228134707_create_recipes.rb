class CreateRecipes < ActiveRecord::Migration[7.0]
  def change
    create_table :recipes do |t|
      t.string :title
      t.integer :servings
      t.integer :prepare_time
      t.text :description

      t.timestamps
    end
  end
end
