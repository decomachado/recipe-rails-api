class CreateCommentUpvotes < ActiveRecord::Migration[7.0]
  def change
    create_table :comment_upvotes do |t|
      t.integer :value

      t.timestamps
    end
  end
end
