10.times do 
    first_name = Faker::Name.first_name
    User.create({
        first_name: first_name,
        last_name: Faker::Name.last_name,
        email: Faker::Internet.free_email(name: first_name),
        password: "password",
        password_confirmation: "password",
        birth: Faker::Date.between(from: 100.years.ago, to: 15.years.ago)
    })
end

40.times do
    Recipe.create({
        user: User.find(Faker::Number.between(from: 1, to: 10)),
        title: Faker::Food.dish,
        servings: Faker::Number.between(from: 1, to: 15),
        prepare_time: Faker::Number.between(from: 25, to: 150),
        description: Faker::Lorem.sentence(word_count: 200)
    })
end

40.times do
    Ingredient.create({
        name: Faker::Food.ingredient,
        description: Faker::Lorem.sentence(word_count: 40)
    })
end

450.times do 
    RecipeIngredient.create({
        recipe: Recipe.find(Faker::Number.between(from: 1, to: 40)),
        ingredient: Ingredient.find(Faker::Number.between(from: 1, to: 40)),
        amount: Faker::Measurement.volume
    })
end

750.times do 
    Comment.create({
        title: Faker::Lorem.sentence(word_count: Faker::Number.between(from: 2, to: 6)),
        content: Faker::Lorem.sentence(word_count: Faker::Number.between(from: 25, to: 60)),
        rating: Faker::Number.between(from: 0, to: 10),
        user: User.find(Faker::Number.between(from: 1, to: 10)),
        recipe: Recipe.find(Faker::Number.between(from: 1, to: 40))
    })
end

2500.times do 
    num = 0
    loop do
        num = Faker::Number.between(from: -1, to: 1)
        break if num != 0
    end

    CommentUpvote.create({
        value: num,
        user: User.find(Faker::Number.between(from: 1, to: 10)),
        comment: Comment.find(Faker::Number.between(from: 1, to: 750))
    })
end