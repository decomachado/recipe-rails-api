Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      resources :sessions, only: [:create]
      resources :users, only: [:create, :destroy, :show, :index, :update]
      resources :recipes, only: [:create, :index, :show, :destroy]
      resources :ingredients, only: [:index, :show]
      resources :comments, only: [:create, :destroy, :show]
      resources :comment_upvotes, only: [:create, :destroy]
      delete :logout, to: "sessions#logout"
      get :logged_in, to: "sessions#logged_in"
    end
  end
end
